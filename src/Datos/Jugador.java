package Datos;

public class Jugador {
    private String nombreUsuario;
    private String nombre;
    private String apellido;
    private int edad;
    private String pais;

    public Jugador() {
        this("","","",0,"");
    }

    public Jugador(String nombreUsuario, String nombre, String apellido, int edad, String pais) {
        this.nombreUsuario = nombreUsuario;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.pais = pais;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String guardar() {
        return nombreUsuario + ";" + nombre + ";" + apellido + ";" + edad + ";" + pais + "\n";
    }

    @Override
    public String toString() {
        return nombreUsuario;
    }

    public String toStringCompleto() {
        return "Jugador: \n" +
                "Nombre de usuario: " + nombreUsuario + "\n" +
                "Nombre: " + nombre + "\n" +
                "Apellido: " + apellido + "\n" +
                "Edad: " + edad + "\n" +
                "País: " + pais + "\n";
    }
}
