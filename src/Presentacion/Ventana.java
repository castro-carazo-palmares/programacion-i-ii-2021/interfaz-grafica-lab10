package Presentacion;

import Datos.Jugador;
import Negocio.Escritura;
import Negocio.Lectura;

import javax.swing.*;
import java.util.List;

public class Ventana {
    private JPanel panel;
    private JComboBox cboJugadores;
    private Lectura lector;
    private List<Jugador> listaJugadores;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        lector = new Lectura();

        lector.abrir();
        listaJugadores = lector.leer();
        lector.cerrar();

        for (Jugador jugador : listaJugadores) {
            cboJugadores.addItem(jugador);
        }

        cboJugadores.setBounds(30, 30, 200, 60);

    }

    public static void main(String[] args) {
        new Ventana();
    }
}
