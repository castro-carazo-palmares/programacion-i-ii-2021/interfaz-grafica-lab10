package Negocio;

import Datos.Jugador;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Escritura {

    private BufferedWriter escritor;

    public void abrir() {
        try {
            escritor = new BufferedWriter(new FileWriter("jugadores.txt", true));
        } catch (IOException ioException) {
            System.err.println("Error al crear el archivo");
        }
    }

    public void escribir(Jugador jugador) {
        try {
            escritor.write(jugador.guardar());
        } catch (IOException ioException) {
            System.err.println("Error al escribir en el archivo");
        }
    }

    public void cerrar() {
        try {
            if (escritor != null) {
                escritor.close();
            }
        } catch (IOException ioException) {
            System.err.println("Error al cerrar el archivo");
        }
    }

}
